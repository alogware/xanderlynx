var embed = {};

var timeRegex = /^(\d+h)?(\d+m)?(\d+s)?$/;
var fixStartTime = function (qs) {
  if (!qs) return "";

  return qs
    .split("&")
    .map((q) => {
      /** @type {string[]} */
      var [name, ...value] = q.split("=");
      value = value.join("=");
      if (name != "t") {
        return q;
      }

      var m = value.match(timeRegex);
      if (!m || !m[0]) {
        return "start=" + value;
      }

      var totalStart = 0;

      if (m[1]) {
        totalStart +=
          window.parseInt(m[1].substring(0, m[1].length - 1)) * 3600;
      }

      if (m[2]) {
        totalStart += window.parseInt(m[2].substring(0, m[2].length - 1)) * 60;
      }

      if (m[3]) {
        totalStart += window.parseInt(m[3].substring(0, m[3].length - 1));
      }

      return "start=" + totalStart;
    })
    .join("&");
};

var generateYoutubeEmbed = function (_, vid, qs) {
  return (
    "https://www.youtube-nocookie.com/embed/" + vid + "?" + fixStartTime(qs)
  );
};

var generateInvidiousEmbed = function (_, vid, qs) {
  return "https://invidio.us/embed/" + vid + "?" + fixStartTime(qs);
};

var generateBitchuteEmbed = function (_, vid) {
  return "https://www.bitchute.com/embed/" + vid;
};

var generateOdyseeEmbed = function(_, vid) {
  return "https://odysee.com/$/embed/" + vid;
}

var embedMap = new Map([
  [/youtube\.com\/watch\?v=([^&]+)(?:&(.*))?$/, generateYoutubeEmbed],
  [/youtu\.be\/([^?]+)(?:\?(.*))?$/, generateYoutubeEmbed],
  [/bitchute\.com\/video\/([^\/]+)\//, generateBitchuteEmbed],
  [/odysee\.com\/@[^\/]+\/(.*)$/, generateOdyseeEmbed],
]);

embed.generateEmbedHTML = function (link) {
  return (
    '<iframe width="480" height="270" ' +
    'src="' +
    link +
    '" frameborder="0" allowfullscreen></iframe>'
  );
};

embed.init = function () {
  var messageElements = document.getElementsByClassName("divMessage");

  for (var i = 0; i < messageElements.length; ++i) {
    var linkElements = messageElements[i].getElementsByTagName("a");

    for (var j = 0; j < linkElements.length; ++j)
      embed.processLinkForEmbed(linkElements[j]);
  }
};

embed.processLinkForEmbed = function (link) {
  Array.from(embedMap.entries()).some(function (data) {
    var regexp = data[0],
      f = data[1];

    if (!regexp.test(link.href)) return false;

    link.style.display = "inline";

    var embedLink = f.apply(null, link.href.match(regexp));
    var iframe = embed.generateEmbedHTML(embedLink);

    var embedCtr = document.createElement("div");
    embedCtr.style.display = "inline";

    var embedButton = document.createElement("span");
    embedButton.textContent = "[Embed]";
    embedButton.className = "embedButton glowOnHover";
    embedCtr.append(embedButton);

    var iframeCtr = document.createElement("div");
    iframeCtr.style.display = "none";
    embedCtr.append(iframeCtr);

    var active = false;
    embedButton.addEventListener("click", function () {
      active = !active;

      if (active) {
        iframeCtr.style.display = "block";
        embedButton.textContent = "[Remove]";
        iframeCtr.innerHTML = iframe;
      } else {
        iframeCtr.style.display = "none";
        embedButton.textContent = "[Embed]";
        iframeCtr.innerHTML = "";
      }
    });

    link.parentElement.insertBefore(embedCtr, link.nextSibling);
    return true;
  });
};

embed.init();
