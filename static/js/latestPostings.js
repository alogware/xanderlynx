var latestPostings = {};

api.mod = true;

latestPostings.init = function() {

  latestPostings.postsDiv = document.getElementById('divPostings');

  var moreButton = document.getElementById('buttonLoadMore');
  moreButton.className = '';
  moreButton.onclick = latestPostings.loadMore;
  latestPostings.moreButton = moreButton;

  var parts = document.getElementById('linkNext').href.split('?')[1].split('&');

  var args = {};

  for (var i = 0; i < parts.length; i++) {
    var subParts = parts[i].split('=');

    args[subParts[0]] = subParts[1];
  }

  latestPostings.latestCheck = new Date(+args.date);

  latestPostings.originalTitle = document.title;

  document.addEventListener("scroll", function() {
    if (document.documentElement.scrollTop < 30 &&
        document.title !== latestPostings.originalTitle)
      document.title = latestPostings.originalTitle;
  }, false);

};

latestPostings.loadMore = function(event) {

  event.preventDefault();

  var origText = latestPostings.moreButton.innerText;
  latestPostings.moreButton.innerText = "Loading...";
  latestPostings.moreButton.disabled = true;

  api.formApiRequest('latestPostings', {}, function gotData(status, data) {

    if (status !== 'ok') {
      latestPostings.moreButton.innerText = origText;
      latestPostings.moreButton.disabled = false;
      return;
    }

    if (data.length) {
      latestPostings.latestCheck = new Date(data[0].creation);
    }

    for (var i = data.length - 1; i >= 0; i--) {

      var post = data[i];

      var cell = posting.addPost(post, post.boardUri, post.threadId, false,
          true);

      latestPostings.postsDiv.insertBefore(cell,
          latestPostings.postsDiv.childNodes[0]);

    }

    if (data.length > 0)
      document.title = "(" + data.length + ") " + latestPostings.originalTitle;

    latestPostings.moreButton.innerText = origText;
    latestPostings.moreButton.disabled = false;

  }, true, {
    date : latestPostings.latestCheck.getTime(),
    boards : document.getElementById('fieldBoards').value
  });

};

latestPostings.init();
