var tooltips = {};
tooltips.linkRegex = /\/(\w+)\/res\/(\d+)\.html(?:\#(\d+))?/;

tooltips.init = function() {

  tooltips.bottomMargin = 25;
  tooltips.loadingPreviews = {};
  tooltips.loadedContent = {};
  tooltips.quoteReference = {};
  tooltips.knownPosts = {};
  tooltips.knownData = {};

  var posts = document.getElementsByClassName('postCell');

  for (var i = 0; i < posts.length; i++) {
    tooltips.addToKnownPostsForBackLinks(posts[i])
  }

  var threads = document.getElementsByClassName('opCell');

  for (i = 0; i < threads.length; i++) {
    tooltips.addToKnownPostsForBackLinks(threads[i])
  }

  var quotes = document.getElementsByClassName('quoteLink');

  for (i = 0; i < quotes.length; i++) {
    tooltips.processQuote(quotes[i]);
  }

  tooltips.cacheExistingHTML('innerOP');
  tooltips.cacheExistingHTML('innerPost');
};

tooltips.cacheExistingHTML = function(className) {
  [].slice.call(document.getElementsByClassName(className))
    .forEach(tooltips.cacheSinglePost);
};

tooltips.cacheSinglePost = function(posting) {
  const post = posting.cloneNode(true);
  post.className = "innerPost";
  const checkbox = post.querySelector(".deletionCheckBox");
  if (checkbox) checkbox.remove();

  const { href } = post.querySelector('.linkSelf');
  tooltips.loadedContent[href] = post;
};

tooltips.addToKnownPostsForBackLinks = function(posting) {

  var postBoard = posting.dataset.boarduri;

  var list = tooltips.knownPosts[postBoard] || {};
  tooltips.knownPosts[postBoard] = list;

  list[posting.id] = {
    added : [],
    container : posting.querySelector('.panelBacklinks'),
    cell : posting
  };
};

tooltips.addBackLink = function(quoteUrl, quote) {

  var matches = quoteUrl.match(tooltips.linkRegex);

  var board = matches[1];
  var thread = matches[2];
  var post = matches[3];

  var knownBoard = tooltips.knownPosts[board];

  if (knownBoard) {

    var knownBackLink = knownBoard[post];

    if (knownBackLink) {

      var containerPost = quote.parentNode.parentNode;

      while (!containerPost.classList.contains('postCell')
          && !containerPost.classList.contains('opCell')) {
        containerPost = containerPost.parentNode;
      }

      var sourceBoard = containerPost.dataset.boarduri;
      var sourcePost = containerPost.id;

      var sourceId = sourceBoard + '_' + sourcePost;

      if (knownBackLink.added.indexOf(sourceId) > -1) {
        return;
      } else {
        knownBackLink.added.push(sourceId);
      }

      var innerHTML = '>>';

      if (sourceBoard != board) {
        innerHTML += '/' + containerPost.dataset.boarduri + '/';
      }

      innerHTML += sourcePost;

      var backLink = document.createElement('a');
      backLink.innerHTML = innerHTML;

      var backLinkUrl = '/' + sourceBoard + '/res/' + thread + '.html#'
          + sourcePost;

      backLink.href = backLinkUrl;

      knownBackLink.container.appendChild(backLink);

      knownBackLink.container.appendChild(document.createTextNode(' '));

      tooltips.processQuote(backLink, true);

      // update the cached html to add the new tooltip
      if (knownBackLink.cell.classList.contains("opCell")) {
        tooltips.cacheSinglePost(knownBackLink.cell.querySelector(".innerOP"));
      } else {
        tooltips.cacheSinglePost(
          knownBackLink.cell.querySelector(".innerPost, .markedPost")
        );
      }

    }

  }

};

tooltips.checkHeight = function(tooltip) {

  var windowHeight = document.documentElement.clientHeight + window.scrollY;

  if (tooltip.offsetHeight + tooltip.offsetTop + tooltips.bottomMargin > windowHeight) {
    tooltip.style.top = (windowHeight - tooltip.offsetHeight - tooltips.bottomMargin)
        + 'px';
  }

}

tooltips.processQuote = function(quote, backLink) {

  var tooltip;

  var quoteUrl = quote.href;

  {
    // add (OP) link if anchor ID == thread ID

    // climb to opCell
    let cell = quote;
    const match = quoteUrl.match(tooltips.linkRegex);

    while (!cell.classList.contains("opCell")) {
      cell = cell.parentElement;
      if (!cell) {
        // This is probably a post that was just created via thread autoRefresh.
        // check current page url.
        const pageMatch = location.pathname.match(tooltips.linkRegex);
        if (!pageMatch) {
          cell = null;
          break;
        }

        // board uri === page board uri && quoted post === page thread id
        if (match[1] === pageMatch[1] && match[3] === pageMatch[2])
          quote.classList.add("op");

        break;
      }
    }

    if (cell) {
      // try to match board uri == quote uri and thread id == quoted post id
      if (match[1] === cell.dataset.boarduri && match[3] === cell.id)
        quote.classList.add("op");
    }
  }


  if (!backLink) {
    tooltips.addBackLink(quoteUrl, quote);
  }

  let highlightedPost = null;

  quote.onmouseenter = function() {
    // check if the post is within the page bounds first.
    if (tooltips.loadedContent[quoteUrl]) {
      const match = quoteUrl.match(tooltips.linkRegex);
      if (match) {
        const post = document.querySelector(
          "[data-boarduri='" + match[1] + "'][id='" + match[3] + "']");
        // make sure the post isn't OP
        if (post && !post.classList.contains("opCell")) {
          // check bounding box of post. if within the viewport, highlight.
          const box = post.getBoundingClientRect();
          if (box.top > 0 && box.bottom < window.innerHeight) {
            highlightedPost = post;
            post.firstElementChild.classList.remove("innerPost");
            post.firstElementChild.classList.add("markedPost");
            return;
          }
        }
      }
    }

    tooltip = document.createElement('div');
    tooltip.className = 'quoteTooltip';

    document.body.appendChild(tooltip);

    var rect = quote.getBoundingClientRect();

    var previewOrigin = {
      x : rect.right + 10 + window.scrollX,
      y : rect.top + window.scrollY
    };

    tooltip.style.left = previewOrigin.x + 'px';
    tooltip.style.top = previewOrigin.y + 'px';
    tooltip.style.display = 'inline';

    if (tooltips.loadedContent[quoteUrl]) {
      tooltip.appendChild(tooltips.loadedContent[quoteUrl]);

      tooltips.checkHeight(tooltip);

    } else {
      tooltip.innerHTML = 'Loading';
    }

    if (!tooltips.loadedContent[quoteUrl]
        && !tooltips.loadingPreviews[quoteUrl]) {
      tooltips.loadQuote(tooltip, quoteUrl);
    }

    if (!api.isBoard) {
      var matches = quote.href.match(/\#(\d+)/);

      quote.onclick = function() {
        thread.markPost(matches[1]);
      };
    }

  };

  quote.onmouseout = function() {
    if (highlightedPost) {
      highlightedPost.firstElementChild.classList.remove("markedPost");
      highlightedPost.firstElementChild.classList.add("innerPost");
      highlightedPost = null;
      return;
    }

    if (tooltip) {
      tooltip.remove();
      tooltip = null;
    }
  };

};

tooltips.generateHTMLFromData = function(postingData, tooltip, quoteUrl) {

  if (!postingData) {
    tooltip.innerHTML = 'Not found';
    return;
  }

  var tempDiv = posting.addPost(postingData, postingData.boardUri,
      postingData.threadId, true).getElementsByClassName('innerPost')[0];

  tempDiv.getElementsByClassName('deletionCheckBox')[0].remove();

  // lol stephenlynx, you're so cute.
  tooltip.innerHTML = tempDiv.outerHTML;

  tooltips.checkHeight(tooltip);

  tooltips.loadedContent[quoteUrl] = tempDiv;

};

tooltips.cacheData = function(threadData) {

  for (var i = 0; i < threadData.posts.length; i++) {
    var postData = threadData.posts[i];
    tooltips.knownData[threadData.boardUri + '/' + postData.postId] = postData;
  }

  tooltips.knownData[threadData.boardUri + '/' + threadData.threadId] = threadData;

};

tooltips.loadQuote = function(tooltip, quoteUrl) {

  var matches = quoteUrl.match(/\/(\w+)\/res\/(\d+)\.html\#(\d+)/);

  var board = matches[1];
  var thread = +matches[2];
  var post = +matches[3];

  var postingData = tooltips.knownData[board + '/' + post];

  if (postingData) {
    tooltips.generateHTMLFromData(postingData, tooltip, quoteUrl);
    return;
  }

  var threadUrl = '/' + board + '/res/' + thread + '.json';

  tooltips.loadingPreviews[quoteUrl] = true;

  api.localRequest(threadUrl, function receivedData(error, data) {

    delete tooltips.loadingPreviews[quoteUrl];

    if (error) {
      tooltip.innerHTML = 'Not found';
      return;
    }

    tooltips.cacheData(JSON.parse(data));

    tooltips.generateHTMLFromData(tooltips.knownData[board + '/' + post],
        tooltip, quoteUrl);

  });

};

tooltips.init();
