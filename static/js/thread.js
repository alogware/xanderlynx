var thread = {};

thread.init = function() {
  api.mod = !!document.getElementById("divMod");

  api.mod = !!document.getElementById('divMod');

  api.hiddenCaptcha = !document.getElementById('captchaDiv');

  document.getElementById('checkboxChangeRefresh').onchange = thread.changeRefresh;

  document.getElementsByTagName('body')[0].onscroll = function() {
    if (!thread.unreadPosts) {
      return;
    }

    var rect = thread.lastPost.getBoundingClientRect();

    if (rect.bottom < window.innerHeight) {
      thread.unreadPosts = 0;

      document.title = thread.originalTitle;
    }
  };

  api.boardUri = document.getElementById("boardIdentifier").value;
  thread.divPosts = document.getElementsByClassName("divPosts")[0];

  thread.initThread();

  document.getElementsByClassName("divRefresh")[0].style.display = "inline-block";

  thread.messageLimit = +document.getElementById("labelMessageLength")
    .innerHTML;
  thread.refreshLabel = document.getElementById("labelRefresh");

  thread.refreshButton = document.getElementById("refreshButton");

  thread.refreshButton.onclick = function() {
    thread.refreshPosts(true)
  };

  thread.refreshButton.onclick = function() {
    thread.refreshPosts(true)
  };

  if (document.getElementById('divArchive')) {
    api.convertButton('archiveFormButon', thread.archiveThread, 'archiveField');
  }

  if (document.getElementById('divMerge')) {
    api.convertButton('mergeFormButton', thread.mergeThread, 'mergeField');
  }

  if (document.getElementById("controlThreadIdentifier")) {
    api.convertButton(
      "settingsFormButon",
      thread.saveThreadSettings,
      "threadSettingsField"
    );

    if (document.getElementById("ipDeletionForm")) {
      api.convertButton(
        "deleteFromIpFormButton",
        thread.deleteFromIp,
        "ipDeletionField"
      );
    }

    if (document.getElementById("formTransfer")) {
      api.convertButton("transferFormButton", thread.transfer, "transferField");
    }

    api.convertButton('inputBan', posting.banPosts, 'banField');
    api.convertButton('inputIpDelete', posting.deleteFromIpOnBoard);
    api.convertButton('inputThreadIpDelete', posting.deleteFromIpOnThread);
    api.convertButton('inputSpoil', posting.spoilFiles);
  }

  thread.replyButton = document.querySelector(".post-submit");
  thread.replyButton.disabled = false;

  api.convertButton(thread.replyButton, thread.postReply);

  var replies = document.getElementsByClassName("postCell");

  if (replies && replies.length) {
    thread.lastReplyId = replies[replies.length - 1].id;
  }

  api.localRequest('/' + api.boardUri + '/res/' + api.threadId + '.json',
      function(error, data) {

        if (error) {
          return thread.changeRefresh();
        }

        try {
          data = JSON.parse(data);
        } catch (error) {
          return thread.changeRefresh();
        }

        thread.wssPort = data.wssPort;
        thread.wsPort = data.wsPort;
        thread.changeRefresh();

      });

  var postingQuotes = document.getElementsByClassName("linkQuote");

  for (var i = 0; i < postingQuotes.length; i++) {
    thread.processPostingQuote(postingQuotes[i]);
  }

  postCommon.addSubmitShortcut(document.getElementById("fieldMessage"));

  thread.initCounters();
  thread.initReplyButton();
};

thread.initThread = function() {
  if (thread.retryTimer) {
    clearInterval(thread.retryTimer);
    delete thread.retryTimer;
  }
  thread.expectedPosts = [];
  thread.lastReplyId = 0;
  thread.originalTitle = document.title;
  posting.highLightedIds = [];
  posting.idsRelation = {};

  var ids = document.getElementsByClassName('labelId');

  for (i = 0; i < ids.length; i++) {
    posting.processIdLabel(ids[i]);
  }

  thread.unreadPosts = 0;
  api.threadId = +document.getElementsByClassName("opCell")[0].id;
  thread.refreshURL = "/" + api.boardUri + "/res/" + api.threadId + ".json";
  thread.refreshParameters = {
    boardUri: api.boardUri,
    threadId: api.threadId
  };
};

thread.transfer = function() {
  var informedBoard = document
    .getElementById("fieldDestinationBoard")
    .value.trim();

  api.formApiRequest('transferThread', {
    boardUri : api.boardUri,
    threadId : api.threadId,
    boardUriDestination : informedBoard
  },
      function setLock(status, data) {

        if (status === 'ok') {
          window.location.pathname = '/' + informedBoard + '/res/' + data
              + '.html';
        } else {
          alert(status + ': ' + JSON.stringify(data));
        }
      });
};

thread.markPost = function(id) {
  if (isNaN(id)) {
    return;
  }

  if (thread.markedPosting && thread.markedPosting.className === "markedPost") {
    thread.markedPosting.className = "innerPost";
  }

  var container = document.getElementById(id);

  if (!container || container.className !== "postCell") {
    return;
  }

  thread.markedPosting = container.getElementsByClassName("innerPost")[0];

  if (thread.markedPosting) {
    thread.markedPosting.className = "markedPost";
  }
};

thread.processPostingQuote = function(link) {
  link.onclick = function() {
    qr.showQr(link.href.match(/#q(\d+)/)[1]);
  };
};

thread.mergeThread = function() {

  var informedThread = document.getElementById("fieldDestinationThread").value
      .trim();

  var destinationThread = document.getElementById("fieldDestinationThread").value;

  api.formApiRequest('mergeThread', {
    boardUri : api.boardUri,
    threadSource : api.threadId,
    threadDestination : destinationThread
  }, function setLock(status, data) {

    if (status === 'ok') {
      window.location.pathname = '/' + api.boardUri + '/res/'
          + destinationThread + '.html';
    } else {
      alert(status + ': ' + JSON.stringify(data));
    }
  });

};

thread.archiveThread = function() {
  if (!document.getElementById("checkboxArchive").checked) {
    alert("You must confirm that you wish to archive this thread.");
    return;
  }

  api.formApiRequest(
    "archiveThread",
    {
      confirmation: true,
      boardUri: api.boardUri,
      threadId: api.threadId
    },
    function archived(status, data) {
      if (status === "ok") {
        api.resetIndicators({
          locked: document.getElementsByClassName("lockIndicator").length,
          pinned: document.getElementsByClassName("pinIndicator").length,
          cyclic: document.getElementsByClassName("cyclicIndicator").length,
          archived: true
        });
      } else {
        alert(status + ": " + JSON.stringify(data));
      }
    }
  );
};

thread.saveThreadSettings = function() {
  var pinned = document.getElementById("checkboxPin").checked;
  var locked = document.getElementById("checkboxLock").checked;
  var cyclic = document.getElementById("checkboxCyclic").checked;

  api.formApiRequest(
    "changeThreadSettings",
    {
      boardUri: api.boardUri,
      threadId: api.threadId,
      pin: pinned,
      lock: locked,
      cyclic: cyclic
    },
    function setLock(status, data) {
      if (status === "ok") {
        api.resetIndicators({
          locked: locked,
          pinned: pinned,
          cyclic: cyclic,
          archived: document.getElementsByClassName("archiveIndicator").length
        });
      } else {
        alert(status + ": " + JSON.stringify(data));
      }
    }
  );
};

thread.replyCallback = function(status, data) {
  if (status === "ok") {
    postCommon.storeUsedPostingPassword(api.boardUri, api.threadId, data);
    postCommon.addYou(api.boardUri, data);

    document.getElementById("fieldMessage").value = "";
    document.getElementById("fieldSubject").value = "";
    qr.clearQRAfterPosting();
    postCommon.clearSelectedFiles();

    if (!thread.autoRefresh || !thread.socket) {
      thread.refreshPosts(true);
    }
  } else {
    alert(status + ": " + JSON.stringify(data));
  }
};

thread.replyCallback.stop = function() {
  thread.replyButton.innerHTML = thread.originalButtonText;

  qr.setQRReplyText(thread.originalButtonText);

  thread.replyButton.disabled = false;
  qr.setQRReplyEnabled(true);
};

thread.replyCallback.progress = function(info) {
  if (info.lengthComputable) {
    var newText =
      "Uploading " + Math.floor((info.loaded / info.total) * 100) + "%";
    thread.replyButton.innerHTML = newText;

    qr.setQRReplyText(newText);
  }
};

thread.refreshCallback = function(error, receivedData) {

  if ((api.mod && (error !== 'ok')) || (!api.mod && error)) {
    return;
  }

  if (!api.mod) {
    receivedData = JSON.parse(receivedData);
  }

  if (receivedData.threadId !== api.threadId) {

    window.location.href = '/' + receivedData.boardUri + '/res/'
        + receivedData.threadId + '.html';

    return;
  }

  if (thread.fullRefresh) {
    thread.lastReplyId = 0;
    thread.unreadPosts = 0;
    while (thread.divPosts.firstChild) {
      thread.divPosts.removeChild(thread.divPosts.firstChild);
    }

    document.title = thread.originalTitle;
  }

  thread.wsPort = receivedData.wsPort;
  thread.wssPort = receivedData.wssPort;
  tooltips.cacheData(receivedData);

  var posts = receivedData.posts;

  var foundPosts = false;

  if (posts && posts.length) {
    var lastReceivedPost = posts[posts.length - 1];

    if (lastReceivedPost.postId > thread.lastReplyId) {
      foundPosts = true;

      var uids = [];

      for (var i = 0; i < posts.length; i++) {
        var post = posts[i];

        if (post.postId > thread.lastReplyId) {
          thread.unreadPosts++;

          if (thread.expectedPosts.indexOf(post.postId) >= 0) {
            thread.expectedPosts.splice(thread.expectedPosts
                .indexOf(post.postId), 1);

          }

          var postCell = posting.addPost(post, api.boardUri, api.threadId);

          thread.counters.replies++;
          thread.counters.files += post.files.length;
          if (post.id) {
            uids.push(post.id);
          }

          postCommon.checkForYou(postCell, post.postId);

          thread.divPosts.appendChild(postCell);

          thread.lastPost = postCell;

          thread.lastReplyId = post.postId;
        }
      }

      if (!thread.fullRefresh) {
        document.title = "(" + thread.unreadPosts + ") " + thread.originalTitle;
      }

      thread.updateLatestUIDs(uids);
      thread.updateCounters();
    }

    if (thread.expectedPosts.length && !thread.retryTimer) {

      thread.expectedPosts = [];

      thread.retryTimer = setTimeout(function() {

        delete thread.retryTimer;

        if (!thread.refreshingThread) {
          thread.refreshPosts();
        }

      }, 10000);
    }
  }

  if (thread.autoRefresh
      && !(!JSON.parse(localStorage.noWs || 'false') && (thread.wsPort || thread.wssPort))) {
    thread.startTimer(thread.manualRefresh || foundPosts ? 5
        : thread.lastRefresh * 2);
  }
};

thread.refreshCallback.stop = function() {
  thread.refreshButton.disabled = false;

  thread.refreshingThread = false;
};

thread.refreshPosts = function(manual, full) {
  if (thread.refreshingThread) {
    return;
  }

  thread.manualRefresh = manual;
  thread.fullRefresh = full;

  if (thread.autoRefresh && manual) {
    clearInterval(thread.refreshTimer);
  }

  thread.refreshButton.disabled = true;

  thread.refreshingThread = true;

  if (api.mod) {
    api.formApiRequest('mod', {}, thread.refreshCallback, true,
        thread.refreshParameters);
  } else {
    api.localRequest(thread.refreshURL, thread.refreshCallback);
  }
};

thread.sendReplyData = function(files, captchaId) {
  var forcedAnon = !document.getElementById("fieldName");
  var hiddenFlags = !document.getElementById("flagsDiv");

  if (!hiddenFlags) {
    var combo = document.getElementById("flagCombobox");

    var selectedFlag = combo.options[combo.selectedIndex].value;

    postCommon.savedSelectedFlag(selectedFlag);
  }

  if (!forcedAnon) {
    var typedName = document.getElementById("fieldName").value.trim();
    localStorage.setItem("name", typedName);
  }

  var typedEmail = document.getElementById("fieldEmail").value.trim();
  var typedMessage = document.getElementById("fieldMessage").value.trim();
  var typedSubject = document.getElementById("fieldSubject").value.trim();
  var typedPassword = document
    .getElementById("fieldPostingPassword")
    .value.trim();

  if (!typedMessage.length && !files.length) {
    alert("A message or a file is mandatory.");
    return;
  } else if (!forcedAnon && typedName.length > 32) {
    alert("Name is too long, keep it under 32 characters.");
    return;
  } else if (typedMessage.length > thread.messageLimit) {
    alert(
      "Message is too long, keep it under " +
        thread.messageLimit +
        " characters."
    );
    return;
  } else if (typedEmail.length > 64) {
    alert("E-mail is too long, keep it under 64 characters.");
    return;
  } else if (typedSubject.length > 128) {
    alert("Subject is too long, keep it under 128 characters.");
    return;
  } else if (typedPassword.length > 8) {
    alert("Password is too long, keep it under 8 characters.");
    return;
  }

  if (!typedPassword) {
    typedPassword = Math.random()
      .toString(36)
      .substring(2, 10);
  }

  localStorage.setItem("deletionPassword", typedPassword);

  var spoilerCheckBox = document.getElementById("checkboxSpoiler");

  var noFlagCheckBox = document.getElementById("checkboxNoFlag");

  thread.originalButtonText = thread.replyButton.innerHTML;
  thread.replyButton.innerHTML = "Uploading 0%";
  qr.setQRReplyText(thread.replyButton.innerHTML);
  thread.replyButton.disabled = true;
  qr.setQRReplyEnabled(false);

  api.formApiRequest(
    "replyThread",
    {
      name: forcedAnon ? null : typedName,
      flag: hiddenFlags ? null : selectedFlag,
      captcha: captchaId,
      subject: typedSubject,
      noFlag: noFlagCheckBox ? noFlagCheckBox.checked : false,
      spoiler: spoilerCheckBox ? spoilerCheckBox.checked : false,
      password: typedPassword,
      message: typedMessage,
      email: typedEmail,
      files: files,
      boardUri: api.boardUri,
      threadId: api.threadId
    },
    thread.replyCallback
  );
};

thread.processFilesToPost = function(captchaId) {
  postCommon.newGetFilesToUpload(function gotFiles(files) {
    thread.sendReplyData(files, captchaId);
  });
};

thread.postReply = function() {

  if (api.hiddenCaptcha) {
    return bypassUtils.checkPass(thread.processFilesToPost);
  }

  var typedCaptcha = document.getElementById('fieldCaptcha').value.trim();

  if (typedCaptcha.length !== 6 && typedCaptcha.length !== 112) {
    alert('Captchas are exactly 6 (112 if no cookies) characters long.');
    return;
  }

  if (typedCaptcha.length == 112) {
    bypassUtils.checkPass(function() {
      thread.processFilesToPost(typedCaptcha);
    });
  } else {
    var parsedCookies = api.getCookies();

    api.formApiRequest('solveCaptcha', {
      captchaId : parsedCookies.captchaid,
      answer : typedCaptcha
    }, function solvedCaptcha(status, data) {

      if (status !== 'ok') {
        alert(status);
        return;
      }

      bypassUtils.checkPass(function() {
        thread.processFilesToPost(parsedCookies.captchaid);
      });

    });
  }
};

thread.transition = function() {

  if (!thread.autoRefresh) {
    return;
  }

  if (thread.wssPort || thread.wsPort) {
    thread.stopWs();
    thread.startWs();
  } else {
    thread.currentRefresh = 5;
  }

};

thread.startTimer = function(time) {
  if (time > 600) {
    time = 600;
  }

  thread.currentRefresh = time;
  thread.lastRefresh = time;
  thread.refreshLabel.innerHTML = thread.currentRefresh;
  thread.refreshTimer = setInterval(function checkTimer() {

    thread.currentRefresh--;

    if (!thread.currentRefresh) {
      clearInterval(thread.refreshTimer);
      thread.refreshPosts();
      thread.refreshLabel.innerHTML = "";
    } else {
      thread.refreshLabel.innerHTML = thread.currentRefresh;
    }
  }, 1000);
};

thread.stopWs = function() {

  if (!thread.socket) {
    return;
  }

  thread.socket.close();
  delete thread.socket;

};

thread.addWsPost = function(data) {

  tooltips.knownData[api.boardUri + '/' + data.postId] = data;

  var postCell = posting.addPost(data, api.boardUri, api.threadId);

  thread.divPosts.appendChild(postCell);

  thread.lastPost = postCell;

  thread.lastReplyId = data.postId;

};

thread.startWs = function() {

  if (typeof (sideCatalog) !== 'undefined' && sideCatalog.loadingThread) {
    return;
  }

  var isOnion = window.location.hostname.endsWith('.onion');

  var protocol = (thread.wssPort && !isOnion) ? 'wss' : 'ws';

  var portToUse = (thread.wssPort && !isOnion) ? thread.wssPort : thread.wsPort;
  
  thread.socket = new WebSocket(protocol + '://' + window.location.hostname
      + ':' + portToUse);

  thread.socket.onopen = function(event) {
    thread.socket.send(api.boardUri + '-' + api.threadId);
  };

  thread.socket.onmessage = function(message) {

    message = JSON.parse(message.data);

    switch (message.action) {
    case 'post': {

      if (message.data) {
        return thread.addWsPost(message.data);
      }

      thread.expectedPosts.push(message.target[0]);

      setTimeout(function() {

        if (!thread.refreshingThread) {
          thread.refreshPosts();
        }
      }, 200);

      break;
    }
    case 'edit': {
      setTimeout(function() {
        thread.refreshPosts(null, true);
      }, 200);
      break;
    }

    case 'unlink':
    case 'delete': {

      for (var i = 0; i < message.target.length; i++) {

        var post = document.getElementById(message.target[i]);

        if (!post) {
          continue;
        }

        if ( message.action ==='unlink') {
          
          var uploads = post.getElementsByClassName('panelUploads')[0]; 
          
          uploads.remove();
          
        } else {

          var info = post.getElementsByClassName('postInfo')[0];

          var deletedLabel = document.createElement('span');
          deletedLabel.innerHTML = '(Deleted)';

          info.insertBefore(deletedLabel, info
              .getElementsByClassName('linkName')[0]);
       
        }

      }

      break;
    }

    }

  };

  thread.socket.onerror = function(error) {
    delete thread.wsPort;
    delete thread.wssPort;
    thread.changeRefresh();
  };

};

thread.changeRefresh = function() {
  thread.autoRefresh = document.getElementById("checkboxChangeRefresh").checked;

  if (!thread.autoRefresh) {
    thread.refreshLabel.innerHTML = '';
    thread.stopWs();
    clearInterval(thread.refreshTimer);
  } else {

    if (!JSON.parse(localStorage.noWs || 'false')
        && (thread.wsPort || thread.wssPort)) {
      thread.startWs();
    } else {
      thread.startTimer(5);
    }

  }
};

thread.deleteFromIp = function() {
  var typedIp = document.getElementById("ipField").value.trim();
  var typedBoards = document.getElementById("fieldBoards").value.trim();

  if (!typedIp.length) {
    alert("An ip is mandatory");
    return;
  }

  api.formApiRequest(
    "deleteFromIp",
    {
      ip: typedIp,
      boards: typedBoards
    },
    function requestComplete(status, data) {
      if (status === "ok") {
        document.getElementById("ipField").value = "";
        document.getElementById("fieldBoards").value = "";

        alert("Postings deleted.");
      } else {
        alert(status + ": " + JSON.stringify(data));
      }
    }
  );
};

thread.getRepliesCount = function(x) {
  return "" + x + " " + (x === 1 ? "reply" : "replies");
};
thread.getFilesCount = function(x) {
  return "" + x + " " + (x === 1 ? "file" : "files");
};
thread.getUIDsCount = function(x) {
  return "" + x + " " + (x === 1 ? "UID" : "UIDs");
};

thread.generateUid = function(uid) {
  return (
    "<span class='labelId' style='background-color: #" +
    uid +
    ";'>" +
    uid +
    "</span>"
  );
};

thread.activateUid = function(uid) {
  var uidText = uid.textContent;

  uid.addEventListener(
    "mouseover",
    function() {
      uid.textContent =
        uidText + " (" + posting.idsRelation[uidText].length + ")";
    },
    false
  );

  uid.addEventListener(
    "mouseout",
    function() {
      uid.textContent = uidText;
    },
    false
  );

  uid.addEventListener(
    "click",
    function() {
      // taken from posting.js
      var index = posting.highLightedIds.indexOf(uidText);
      var array = posting.idsRelation[uidText];

      if (index > -1) {
        posting.highLightedIds.splice(index, 1);
      } else {
        posting.highLightedIds.push(uidText);
      }

      for (var i = 0; i < array.length; i++) {
        var cellToChange = array[i];

        if (cellToChange.className === "innerOP") {
          continue;
        }

        cellToChange.className = index > -1 ? "innerPost" : "markedPost";
      }
    },
    false
  );
};

thread.initCounters = function() {
  var replies = document.querySelectorAll(".postCell").length;
  var files = document.querySelectorAll(".uploadCell").length;
  var uidSet = new Set(
    Array.from(document.querySelectorAll(".labelId"))
      .map(function processId(id) {
        return id.textContent;
      })
      .reverse()
  );

  var counterEl = document.createElement("div");
  counterEl.id = "divCounters";
  counterEl.innerHTML =
    "<span id='counterReplies'>" +
    thread.getRepliesCount(replies) +
    "</span>" +
    "<span id='counterFiles'>" +
    thread.getFilesCount(files) +
    "</span>" +
    (uidSet.size
      ? "<input type='checkbox' class='control-checkbox' id='uidbox-checkbox' />" +
        "<label for='uidbox-checkbox'><span id='counterUids' title='View latest UIDs'>" +
        thread.getUIDsCount(uidSet.size) +
        "</span></label>" +
        "<div class='uidBox floatingMenu'><p class='header'>Latest UIDs " +
        "<label for='uidbox-checkbox'>&times;</label></p>" +
        "<div class='uids'>" +
        Array.from(uidSet.values())
          .map(thread.generateUid)
          .join("") +
        "</div></div>"
      : "");

  thread.counters = {
    replies: replies,
    files: files,
    uidSet: uidSet,

    counterEl: counterEl,
    repliesEl: counterEl.querySelector("#counterReplies"),
    filesEl: counterEl.querySelector("#counterFiles"),
    uidsEl: counterEl.querySelector("#counterUids"),
    uidBox: counterEl.querySelector(".uidBox")
  };

  // posting.processIdLabel doesn't work here.
  if (thread.counters.uidBox) {
    Array.from(thread.counters.uidBox.querySelectorAll(".labelId")).forEach(
      thread.activateUid
    );
  }

  document.querySelector("#board-bottom-nav").appendChild(counterEl);
};

thread.updateCounters = function() {
  thread.counters.repliesEl.textContent = thread.getRepliesCount(
    thread.counters.replies
  );
  thread.counters.filesEl.textContent = thread.getFilesCount(
    thread.counters.files
  );

  if (thread.counters.uidSet.size)
    thread.counters.uidsEl.textContent = thread.getUIDsCount(
      thread.counters.uidSet.size
    );
};

var __uid_container = document.createElement("div");
thread.updateLatestUIDs = function(uids) {
  if (!thread.counters.uidBox) return;

  uids.forEach(function(uid) {
    var uids = thread.counters.uidBox.querySelector(".uids");
    if (!thread.counters.uidSet.has(uid)) {
      // add to the front
      thread.counters.uidSet.add(uid);
      __uid_container.innerHTML = thread.generateUid(uid);
      var uidEl = __uid_container.firstElementChild;
      thread.activateUid(uidEl);
      uids.insertBefore(uidEl, uids.firstChild);
    } else {
      // bump the uid to the top
      // lol this qs
      var myUid = uids.querySelector('.labelId[style$="' + uid + ';"]');
      uids.insertBefore(myUid, uids.firstChild);
    }
  });
};

thread.initReplyButton = function() {
  var replyButton = document.createElement("a");
  replyButton.className = "replyToThread";
  replyButton.href = "javascript:void(0)";
  replyButton.innerHTML = "[Reply to Thread]";

  replyButton.addEventListener(
    "click",
    function() {
      qr.showQr("", true);
    },
    false
  );

  var counters = document.querySelector("#divCounters");
  counters.insertBefore(replyButton, counters.firstElementChild);
};

thread.init();
