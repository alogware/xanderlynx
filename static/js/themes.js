var themes = {};

themes.init = function() {
  themes.themes = [
    {
      label: "Yotsuba B",
      id: "default"
    },
    {
      label: "Tomorrow",
      id: "tomorrow"
    }
  ];

  var linkSpan = document.getElementById("navLinkSpan");

  if (linkSpan) {
    linkSpan.appendChild(document.createTextNode(" "));

    var themeSelector = document.createElement("select");
    themeSelector.id = "themeSelector";

    var vanillaOption = document.createElement("option");
    vanillaOption.innerHTML = "Board-specific CSS";
    themeSelector.appendChild(vanillaOption);

    for (var i = 0; i < themes.themes.length; i++) {
      var theme = themes.themes[i];

      var themeOption = document.createElement("option");
      themeOption.innerHTML = theme.label;

      if (theme.id === localStorage.selectedTheme) {
        themeOption.selected = true;
      }

      themeSelector.appendChild(themeOption);
    }

    themeSelector.onchange = function() {
      if (!themeSelector.selectedIndex) {
        if (localStorage.selectedTheme) {
          delete localStorage.selectedTheme;
          themeLoader.load();
        }

        return;
      }

      var selectedTheme = themes.themes[themeSelector.selectedIndex - 1];

      if (selectedTheme.id === localStorage.selectedTheme) {
        return;
      }

      localStorage.selectedTheme = selectedTheme.id;

      themeLoader.load();
    };

    linkSpan.appendChild(themeSelector);
  }
};

setTimeout(themes.init, 16);
